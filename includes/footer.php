     <!-- #footer-wrapper start -->
    <div id="footer-wrapper">
        <!-- #footer start -->
        <footer id="footer">
            <!-- .container start -->
            <div class="container">
                <!-- .row start -->
                <div class="row">
                    <!-- .footer-widget-container start -->
                    <ul class="col-md-3 footer-widget-container">
                        <!-- .widget.widget_text -->
                        <li class="widget widget_newsletterwidget">
                            <div class="title">
                                <h3>newsletter subscribe</h3>
                            </div>

                            <p>
                                Want to subscribe to our newsletter feed?
                                Please fill the form below.
                            </p>

                            <br />

                            <form class="newsletter">
                                <input class="email" type="email" placeholder="Your email...">
                                <input type="submit" class="submit" value="">
                            </form>
                        </li><!-- .widget.widget_newsletterwidget end -->
                    </ul><!-- .col-md-3.footer-widget-container end -->

                    <!-- .footer-widget-container start -->
                    <ul class="col-md-3 footer-widget-container">
                        <!-- .widget_pages start -->
                        <li class="widget widget_pages">
                            <div class="title">
                                <h3>company information</h3>
                            </div>

                            <ul>
                                <li><a href="aboutus.php">About us</a></li>
                                <li><a href="services.php">Our services</a></li>
                                <li><a href="web-development.php">Web Development</a></li>
                                <li><a href="portfolio.php">Latest projects</a></li>
                                <li><a href="contact.php">Contact</a></li>
                            </ul>
                        </li><!-- .widget.widget_pages end -->
                    </ul><!-- .footer-widget-container end -->

                    <!-- .footer-widget-container start -->
                    <ul class="col-md-3 footer-widget-container">
                        <!-- #tweetscroll-wrapper start -->
                        <li class="widget" id="tweetscroll-wrapper">
                            <div class="title">
                                <h3>Follow us</h3>
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fxenerit%2F&tabs=timeline&width=300&height=250&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=271772883476803" width="280" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                            </div>
                            
                        </li><!-- .widget#tweetscroll-wrapper end -->
                    </ul><!-- .col-md-3.footer-widget-container end -->

                    <!-- .footer-widget-container start -->
                    <ul class="col-md-3 footer-widget-container">
                        <!-- .widget.contact-info start -->
                        <li class="widget contact-info">
                            <div class="title">
                                <h3>get in touch</h3>
                            </div>

                            <ul class="contact-info-list">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <strong>Address: </strong>House 13, Road 4, Nikunja 2, Dhaka, Bangladesh
                                </li>

                                <li>
                                    <i class="fa fa-phone"></i>
                                    <strong>Phone No: </strong>+88 01711574805 / +88 01611574805
                                </li>

                                <li>
                                    <i class="fa fa-envelope-o"></i>
                                    <strong>Email: </strong><a href="mailto:support@xenerit.com">support@xenerit.com</a>
                                </li>
                            </ul>
                        </li><!-- .widget.contact-info end -->
                    </ul><!-- .footer-widget-container end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </footer><!-- #footer end -->

        <a href="#" class="scroll-up">Scroll</a>
    </div><!-- #footer-wrapper end -->

    <!-- #copyright-container start -->
    <div id="copyright-container">
        <!-- .container start -->
        <div class="container">
            <!-- .row start -->
            <div class="row">
                <!-- .col-md-6 start -->
                <div class="col-md-6">
                    <p>All Right Reserved By <a href="index.php">Xener IT</a></p>
                </div><!-- .ocl-md-6 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .copyright-container end -->

        <script src="js/jquery-2.1.1.min.js"></script><!-- jQuery library -->
        <script src="js/bootstrap.min.js"></script><!-- .bootstrap script -->
        <script src="js/jquery.srcipts.min.js"></script><!-- modernizr, retina, stellar for parallax -->  
        <script src="js/jquery.tweetscroll.min.js"></script><!-- Tweetscroll plugin -->
        <script src='owl-carousel/owl.carousel.min.js'></script><!-- Carousels script -->
        <script src="js/jquery.dlmenu.min.js"></script><!-- for responsive menu -->
        <script src="js/jquery.matchHeight-min.js"></script><!-- match height on services -->
        <script src="js/include.min.js"></script><!-- custom js functions -->
        <script src="style-switcher/styleSwitcher.min.js"></script><!-- for changing styles -->

        <script>
            /* <![CDATA[ */
            jQuery(document).ready(function ($) {
                'use strict';

                $('.owl-carousel').owlCarousel({
                    items: 1,
                    loop: true,
                    margin: 30,
                    responsiveClass: true,
                    mouseDrag: true,
                    responsive: {
                        0: {
                            items: 1,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            dots: true,
                            nav: false,
                            autoHeight: true
                        },
                        600: {
                            items: 1,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            dots: true,
                            nav: false,
                            autoHeight: true
                        },
                        1000: {
                            items: 1,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            mouseDrag: true,
                            dots: true,
                            nav: false,
                            autoHeight: true
                        }
                    }
                });

                

                $('.match-height').each(function () {
                    $(this).children('.service-box-3').matchHeight({});
                });
            });
            /* ]]> */
        </script>
    </body>
</html>