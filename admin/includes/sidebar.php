    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.php" class="logo"><span><?php echo $company_name_1st_part?><span><?php echo $company_name_2nd_part?></span></span><i class="mdi mdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <!-- Page title -->
                        <ul class="nav navbar-nav list-inline navbar-left">
                            <li class="list-inline-item">
                                <button class="button-menu-mobile open-left">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
                            <li class="list-inline-item">
                                <h4 class="page-title"><?php echo $title;?></h4>
                            </li>
                        </ul>
                        <nav class="navbar-custom">
                            <ul class="list-unstyled topbar-right-menu float-right mb-0">

                                <li>
                                    <button class="btn btn-primary digital-clock btn-trans waves-effect waves-primary w-md m-b-5"></button>
                                    
                                </li>
                                <li style="margin-left: 20px">
                                    <a href="profile.php"><button class="btn btn-primary btn-trans waves-effect waves-primary w-md m-b-5"><?php echo $admin_name;?>'s Profile</button></a>
                                    
                                </li>
                    
                                <li style="margin-left: 20px" >
                                    <a href="logout.php"><button class="btn btn-danger btn-trans waves-effect waves-primary w-md m-b-5">Logout</button></a>
                                    
                                </li>
                                
                           
                                    

                                <!--        <li class="hide-phone">
                                    <form class="app-search">
                                        <input type="text" placeholder="Search..." class="form-control">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </li>-->

                            </ul>
                        </nav>
                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/pic/admin_image/<?php echo $admin_image;?>" alt="user-img" title="<?php echo $admin_name;?>" class="rounded-circle img-thumbnail img-responsive">
                        </div>
                        <h5><a href="#"><?php echo $admin_name;?></a> </h5>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="profile.php" title="Profile" class="text-custom">
                                    <i class="mdi mdi-settings"></i>
                                </a>
                            </li>

                            <li class="list-inline-item">
                                <a href="logout.php" title="logout" style="color:red;">
                                    <i class="mdi mdi-power"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End User -->
                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                            <li class="text-muted menu-title">Navigation</li>
                            <li>
                                <a href="index.php" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span> </a>
                            </li>
                            
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-circle-o"></i> <span> Projects </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="insert_project.php"><i class="fa fa-circle-o"></i>insert Project</a></li>
                                    <li><a href="view_project.php"><i class="fa fa-circle-o"></i>View Project</a></li>
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-circle-o"></i> <span> <?php echo $admin_area_name?>s </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="create_user.php"><i class="fa fa-circle-o"></i>Create <?php echo $admin_area_name?></a></li>
                                    <li><a href="view_user.php"><i class="fa fa-circle-o"></i><?php echo $admin_area_name?> List</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
