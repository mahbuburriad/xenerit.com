<?php
//settings_area_start

//database connection area first
$db_host = "localhost";
$db_username = "root";
$db_password = "";
$db_name = "xenerit";
//database connection area end


//database riadoffi_xenerit end


//xenerit information start
$xenerit_email = "support@xenerit.com";
$xenerit = "Xener IT";
$xenerit_website = "https://xenerit.com";
$xenerit_mailto = "<a href='mailto:$xenerit_email'>$xenerit_email</a>";
$xenerit_link = "<a href='$xenerit_website'>$xenerit</a>";
//xenerit information end

//company information start
$company_name_1st_part="Xener";
$company_name_2nd_part=" IT";
$company_full_name = $company_name_1st_part.$company_name_2nd_part;
$company_email="support@xenerit.com";
$company_mailto = "<a href='mailto:$company_email'>$company_email</a>";
$company_website = "https://xenerit.com";
$company_link = "<a href='$company_website'>$company_full_name</a>";
$company_tag_line = "A Web and App development company";
//company information end

//meta tag information start
$meta_description = "A fully featured admin theme which can be used to build CRM, CMS, etc.";
$meta_author = $company_full_name;
$favicon = "assets/images/favicon.ico";
//meta tag information end

//admin area name start
$admin_area_name = "Admin";
//admin area name end

$local_server = $_SERVER['HTTP_HOST'];


//settings_area_end
?>