-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2019 at 07:45 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xenerit`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_pass` varchar(255) NOT NULL,
  `admin_image` text NOT NULL,
  `admin_contact` varchar(255) NOT NULL,
  `admin_country` text NOT NULL,
  `admin_job` varchar(255) NOT NULL,
  `admin_about` text NOT NULL,
  `admin_confirm_code` text NOT NULL,
  `reset_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_name`, `admin_email`, `admin_pass`, `admin_image`, `admin_contact`, `admin_country`, `admin_job`, `admin_about`, `admin_confirm_code`, `reset_code`) VALUES
(5, 'Admin Demo', 'demo@gmail.com', '123456', 'demo.png', '33456693', 'Bangladesh', 'Developer', '  Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical   ', '', ''),
(13, 'Mahbubur Riad', 'admin@admin.com', 'admin', 'pp.jpg', '33456693', 'Bangladesh', 'Developer', ' Lorem Ipsum', '', ''),
(19, 'Sayem Islam Khan', 'mahbubur.riad@gmail.com', '123456', 'img1.jpg', '017115748055', 'Bangladesh', 'Developer', ' vsdfgdfgdfg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_by` varchar(255) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `dates` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `small_image` varchar(255) NOT NULL,
  `large_image` varchar(255) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_name`, `project_by`, `client_name`, `dates`, `link`, `logo`, `small_image`, `large_image`, `skill`, `details`) VALUES
(1, 'Demo title', 'prject by, project by', 'demo client', '04/19/2019', 'https://bferi.com/product/hair-dry-cap/', '19_04_2019_10_24_57_favicon.png', '19_04_2019_10_24_57_4.jpg', '19_04_2019_10_24_57_Capture.jpg', 'skiles.jsadf, asdlof', 'Drutobazaar is multi vendor website made by wordpress theme with premium plugins. this is an ecommerce website mady ny woocommerce plugin. Clients wants me to add add categories and all things. I added them and make it client satisfictionary website'),
(2, 'Demo titless', 'prject by, project bysssssss', 'asdfasdfasdf', '04/20/2019', 'https://bferi.com/product/hair-dry-cap/', '19_04_2019_19_33_44_Capture.jpg', '19_04_2019_19_33_44_5.jpg', '19_04_2019_19_33_44_2.jpg', 'skiles.jsadf, asdlofss', 'Drutobazaar is multi vendor website made by wordpress theme with premium plugins. this is an ecommerce website mady ny woocommerce plugin. Clients wants me to add add categories and all things. I added them and make it client satisfictionary websitesssssssss');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
