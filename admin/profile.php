<?php
session_start();
$title = "Profile";
include("includes/connection.php");
if(!isset($_SESSION['admin_email'])){
echo "<script>window.open('login.php', '_self')</script>";
}
else{
include("includes/index-header.php");
include("includes/form-css.php");
include("includes/sidebar.php");
    
$page_title = "Instruction";
$page_title_details = "Please controller with unique email and password with all details for keeping controller dashboard safe. if any types of problem please contact with me $xenerit_email";
?>

<div class="row">
    <div class="col-sm-8">
        <div class="bg-picture card-box">
            <div class="profile-info-name">
                <img src="assets/pic/admin_image/<?php echo $admin_image;?>" class="img-thumbnail" alt="profile-image">
               

                <div class="profile-info-detail">
                    <h4 class="m-0"><?php echo $admin_name ?></h4>
                    <p class="text-muted m-b-20"><i><?php echo $admin_job ?></i></p>
                    <p><?php echo $admin_email; ?><br><?php echo $admin_contact; ?></p>
                    <p><?php echo $admin_about ?></p>
                      <a href="update_profile.php?update=<?php echo $admin_id;?>"><button class="btn btn-info waves-effect w-md waves-light m-b-5">Update Profile</button></a>
                       <a  href="update_image.php?update=<?php echo $admin_id;?>"><button class="btn btn-purple waves-effect w-md waves-light m-b-5">Update Image</button></a> 

                    <div class="button-list m-t-20">
                        <button type="button" class="btn btn-facebook btn-sm waves-effect waves-light">
                            <i class="fa fa-facebook"></i>
                        </button>

                        <button type="button" class="btn btn-sm btn-twitter waves-effect waves-light">
                            <i class="fa fa-twitter"></i>
                        </button>

                        <button type="button" class="btn btn-sm btn-linkedin waves-effect waves-light">
                            <i class="fa fa-linkedin"></i>
                        </button>

                        <button type="button" class="btn btn-sm btn-dribbble waves-effect waves-light">
                            <i class="fa fa-dribbble"></i>
                        </button>

                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <!--/ meta -->
        <form method="post" class="card-box">
            <span class="input-icon icon-right">
                <textarea rows="2" class="form-control" placeholder="Post a new message"></textarea>
            </span>
            <div class="p-t-10 pull-right">
                <a class="btn btn-sm btn-primary waves-effect waves-light">Send</a>
            </div>
            <ul class="nav nav-pills profile-pills m-t-10">
                <li>
                    <a href="#"><i class="fa fa-user"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-location-arrow"></i></a>
                </li>
                <li>
                    <a href="#"><i class=" fa fa-camera"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-smile-o"></i></a>
                </li>
            </ul>

        </form>
    </div>
    <div class="col-sm-4">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="mdi mdi-dots-vertical"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Action</a>
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Another action</a>
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Something else</a>
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Separated link</a>
                </div>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Other <?php echo $admin_area_name ?> Members</h4>

            <ul class="list-group m-b-0 user-list">
                <?php
$get_admin = "select * from admins";
$run_admin = mysqli_query($con,$get_admin);
while($row_admin = mysqli_fetch_array($run_admin)){
$admin_id = $row_admin['admin_id'];
$admin_name = $row_admin['admin_name'];
$admin_email = $row_admin['admin_email'];
$admin_image = $row_admin['admin_image'];
$admin_country = $row_admin['admin_country'];
$admin_job = $row_admin['admin_job'];
?>
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <img src="assets/pic/admin_image/<?php echo $admin_image; ?>" alt="<?php echo $admin_name; ?>">
                            
                        </div>
                        <div class="user-desc">
                            <span class="name"><?php echo $admin_name; ?></span>
                            <span class="desc"><?php echo $admin_job; ?></span>
                        </div>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>


        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="mdi mdi-dots-vertical"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Action</a>
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Another action</a>
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Something else</a>
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item">Separated link</a>
                </div>
            </div>

            <h4 class="header-title m-t-0 m-b-30"><i class="mdi mdi-notification-clear-all m-r-5"></i> Upcoming Reminders</h4>

            <ul class="list-group m-b-0 user-list">
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <i class="mdi mdi-circle text-primary"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Meet Manager</span>
                            <span class="desc">February 29, 2016 - 10:30am to 12:45pm</span>
                        </div>
                    </a>
                </li>

                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <i class="mdi mdi-circle text-success"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Project Discussion</span>
                            <span class="desc">February 29, 2016 - 10:30am to 12:45pm</span>
                        </div>
                    </a>
                </li>

                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <i class="mdi mdi-circle text-pink"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Meet Manager</span>
                            <span class="desc">February 29, 2016 - 10:30am to 12:45pm</span>
                        </div>
                    </a>
                </li>

                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <i class="mdi mdi-circle text-muted"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Project Discussion</span>
                            <span class="desc">February 29, 2016 - 10:30am to 12:45pm</span>
                        </div>
                    </a>
                </li>

                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <i class="mdi mdi-circle text-danger"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Meet Manager</span>
                            <span class="desc">February 29, 2016 - 10:30am to 12:45pm</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

    </div>
</div>
<?php 
include("includes/footer.php"); 
include("includes/form_js.php"); 
} ?>
