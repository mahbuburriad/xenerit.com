<?php
session_start();
$title ="Admins List";
include("includes/connection.php");
if(!isset($_SESSION['admin_email'])){
echo "<script>window.open('login.php', '_self')</script>";
}
else{
include("includes/index-header.php");
include("includes/table-css.php");
include("includes/sidebar.php");
    
    $page_title = "Instruction";
    $page_title_details = "Please controller with unique email and password with all details for keeping controller dashboard safe. if any types of problem please contact with me $xenerit_email";
?>


<div class="row">
    <div class="col-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><?php echo $page_title; ?></h4>
            <p class="text-muted font-14 m-b-30">
                <?php echo $page_title_details; ?>
            </p>

            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact No</th>
                        <th>Address</th>
                        <th>Job</th>
                        <th>Modify</th>
                    </tr>
                </thead>


                <tbody>
                    <?php
$get_admin = "select * from admins";
$run_admin = mysqli_query($con,$get_admin);
while($row_admin = mysqli_fetch_array($run_admin)){
$admin_id = $row_admin['admin_id'];
$admin_name = $row_admin['admin_name'];
$admin_email = $row_admin['admin_email'];
$admin_image = $row_admin['admin_image'];
$admin_country = $row_admin['admin_country'];
$admin_job = $row_admin['admin_job'];
?>

                    <tr>
                        <td><img src="assets/pic/admin_image/<?php echo $admin_image; ?>" width="60" height="60"></td>

                        <td>
                            <?php echo $admin_name; ?>
                        </td>

                        <td>
                            <?php echo $admin_email; ?>
                        </td>
                        <td>
                            <?php echo $admin_contact; ?>
                        </td>
                        <td>
                            <?php echo $admin_country; ?>
                        </td>

                        <td>
                            <?php echo $admin_job; ?>
                        </td>

                        <td>

                            <a href="update_user.php?update_user=<?php echo $admin_id; ?>"><i class="fa fa-pencil-square-o"></i></a> / <a href="delete_user.php?delete=<?php echo $admin_id; ?>"><i class="fa fa-trash-o"></i></a>

                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- end row -->

<?php 
include("includes/footer.php"); 
include("includes/table_js.php"); 
    
    
} ?>
