<?php
session_start();
include("includes/connection.php");
include("includes/settings.php"); 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $meta_description?>">
    <meta name="author" content="<?php echo $meta_author?>">
    <link rel="shortcut icon" href="<?php echo $favicon?>">
    <title><?php echo $company_full_name?> Management System | <?php echo $company_full_name?> | <?php echo $xenerit?></title>
    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <script src="assets/js/modernizr.min.js"></script>
</head>
<body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class="text-center">
            <a href="index.php" class="logo"><span><?php echo $company_name_1st_part?><span><?php echo $company_name_2nd_part?></span></span></a>
            <h5 class="text-muted m-t-0 font-600"><?php echo $company_full_name?> Control Panel</h5>
        </div>
        <div class="m-t-40 card-box">
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
            </div>
            <div class="p-20">
                <form class="form-horizontal m-t-20" action="" method="post"> 

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="email" class="form-control" name="admin_email" placeholder="E-mail address" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="password" class="form-control" name="admin_pass" placeholder="Password" required>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-custom">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group text-center m-t-30">
                        <div class="col-xs-12">
                            <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" name="admin_login" type="submit">Log In</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12">
                            <a href="password_recover.php" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!-- end card-box-->

        <!--            <div class="row">
                <div class="col-sm-12 text-center">
                    <p class="text-muted">Don't have an account? <a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                </div>
            </div>-->

    </div>
    <!-- end wrapper page -->
    
    <?php

if(isset($_POST['admin_login'])){
$admin_email = mysqli_real_escape_string($con,$_POST['admin_email']);
$admin_pass = mysqli_real_escape_string($con,$_POST['admin_pass']);
$get_admin = "SELECT * FROM admins WHERE admin_email='$admin_email' AND admin_pass='$admin_pass'"; 
$run_admin = mysqli_query($con,$get_admin);

    $count = mysqli_num_rows($run_admin);

    $row_admin = mysqli_fetch_array($run_admin);
$admin_confirm_code = $row_admin['admin_confirm_code'];

    
    if(($count==1) && empty($admin_confirm_code) ){
$_SESSION['admin_email']=$admin_email;
echo "<script>window.open('index.php','_self')</script>";
}
    elseif(($count==1) && !empty($admin_confirm_code)){
        echo "<script>window.open('confirmation.php','_self')</script>";
    }
else {
echo "<script>alert('Email or Password is Wrong')</script>";
}}
?>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

</body>

</html>
