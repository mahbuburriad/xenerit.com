<?php
session_start();
$title = "Create Admin";
include("includes/connection.php");
if(!isset($_SESSION['admin_email'])){
echo "<script>window.open('login.php', '_self')</script>";
}
else{
include("includes/index-header.php");
include("includes/form-css.php");
include("includes/sidebar.php");
    
    $page_title = "Instruction";
    $page_title_details = "Please controller with unique email and password with all details for keeping controller dashboard safe. if any types of problem please contact with me $xenerit_email";
?>

<!-- Form row -->
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><?php echo $page_title;?></h4>
            <p class="text-muted m-b-30 font-13">
                <?php echo $page_title_details;?>

            </p>

            <form method="post" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4" class="col-form-label"><?php echo $admin_area_name?> Name</label>
                        <input type="text" name="admin_name" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4" class="col-form-label"><?php echo $admin_area_name?> Email</label>
                        <input type="email" name="admin_email" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputAddress" class="col-form-label"><?php echo $admin_area_name?> Password</label>
                        <input type="password" name="admin_pass" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label"><?php echo $admin_area_name?> Address</label>
                        <input type="text" name="admin_country" class="form-control" required>
                    </div>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputCity" class="col-form-label"><?php echo $admin_area_name?> Job/Position</label>
                        <input type="text" name="admin_job" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip" class="col-form-label"><?php echo $admin_area_name?> Contact</label>
                        <input type="tel" name="admin_contact" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip" class="col-form-label"><?php echo $admin_area_name?> Image</label>
                        <input type="file" name="admin_image" class="form-control" required>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputZip" class="col-form-label">About <?php echo $admin_area_name?></label>
                    <textarea name="admin_about" class="form-control" rows="3"> </textarea>

                </div>
                <input type="submit" name="submit" value="Create User" class="btn btn-primary" class="btn btn-primary form-control">
            </form>
        </div>
    </div>
</div>

<?php

if(isset($_POST['submit'])){
$admin_name = $_POST['admin_name'];
$admin_email = $_POST['admin_email'];
$admin_pass = $_POST['admin_pass'];
$admin_country = $_POST['admin_country'];
$admin_job = $_POST['admin_job'];
$admin_contact = $_POST['admin_contact'];
$admin_about = $_POST['admin_about'];
    
/*$admin_image = $_FILES['admin_image']['name'];
$temp_admin_image = $_FILES['admin_image']['tmp_name'];
move_uploaded_file($temp_admin_image,"assets/pic/admin_image/$admin_image");*/
     $target_dir = "assets/pic/admin_image/";
$admin_image = $target_dir .  date('d_m_Y_H_i_s') . '_'. $_FILES["admin_image"]["name"];
    $new = date('d_m_Y_H_i_s') . '_'. $_FILES["admin_image"]["name"];
$uploadOk = 1;
$imageFileType = pathinfo($admin_image, PATHINFO_EXTENSION);
move_uploaded_file($_FILES["admin_image"]["tmp_name"], $admin_image);
    
$get_email = "SELECT * FROM admins WHERE admin_email='$admin_email'";
                               $run_email = mysqli_query($con, $get_email);
                               $check_email = mysqli_num_rows($run_email);
                               if($check_email == 1){
                                   echo "<script>alert('This Email is already registered ! please choose another email')</script>";
                                   exit();
                               }
    
$admin_confirm_code = mt_rand();
    
//email-address confirmation start
include("includes/email_template.php");
        
//email confirmation end
    

$insert_admin = "insert into admins (admin_name,admin_email,admin_pass,admin_image,admin_contact,admin_country,admin_job,admin_about, admin_confirm_code) values ('$admin_name','$admin_email','$admin_pass','$new','$admin_contact','$admin_country','$admin_job','$admin_about', '$admin_confirm_code')";
$run_admin = mysqli_query($con,$insert_admin);

if($run_admin){
echo "<script>window.open('view_user.php','_self')</script>";
}else
{
   echo"<script>alert('Error')</script>" ;
}}?>
<!-- end row -->

<?php 
include("includes/footer.php"); 
include("includes/form_js.php"); 
} ?>
