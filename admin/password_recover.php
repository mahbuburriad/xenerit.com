<?php
include("includes/connection.php");
include("includes/settings.php");

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <title><?php echo $company_full_name ?> - <?php echo $company_tag_line?></title>

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

</head>

<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class="text-center">
            <a href="index.php" class="logo"><span><?php echo $company_name_1st_part ?><span><?php echo $company_name_2nd_part ?></span></span></a>
            <h5 class="text-muted m-t-0 font-600"><?php echo $company_tag_line?></h5>
        </div>
        <div class="m-t-40 card-box">
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Reset Password</h4>

                <p class="text-muted m-b-0 font-13 m-t-20">Enter your email address and we'll send you an email with instructions to reset your password. </p>
            </div>
            <div class="p-20">
                <form class="form-horizontal m-t-20" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="email" name="admin_email" placeholder="Enter email">
                        </div>
                    </div>

                    <div class="form-group text-center m-t-20 m-b-0">
                        <div class="col-xs-12">
                            <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" name="submit" type="submit">Send Email</button>
                        </div>
                    </div>

                    <h1></h1>

                </form>

            </div>
        </div>
        <!-- end card-box -->

        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="text-muted">Already have account?<a href="login.php" class="text-primary m-l-5"><b>Sign In</b></a></p>
            </div>
        </div>

    </div>
    <!-- end wrapper page -->


    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <?php
if(isset($_POST['submit'])){
$admin_emails = $_POST['admin_email'];
 
$get_admins = "select admin_email, admin_id from admins where admin_email = '$admin_emails'";
$run_admins = mysqli_query($con,$get_admins);
$count = mysqli_num_rows($run_admins); //email is exist or not
    
if($count==1){
    
    
$reset_code = mt_rand();
    
$update_admins = "update admins set reset_code = '$reset_code' where admin_email = '$admin_emails'";
$run_admins = mysqli_query($con,$update_admins);

    
    
$get_admin = "select admin_email, reset_code from admins where admin_email = '$admin_emails'";
$run_admin = mysqli_query($con,$get_admin);
$row_admin = mysqli_fetch_array($run_admin);
$admin_email = $row_admin['admin_email'];
$reset_code = $row_admin['reset_code'];
    
include("includes/forget_template.php");
    echo "<script>window.open('login.php','_self')</script>";
    
}

else{
        echo "<script>alert('Please enter registered valid email')</script>";
    }
}
            
    
    ?>
</body>

</html>
