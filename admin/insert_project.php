<?php
session_start();
$title = "Insert Project";
include("includes/connection.php");
if(!isset($_SESSION['admin_email'])){
echo "<script>window.open('login.php', '_self')</script>";
}
else{
include("includes/index-header.php");
include("includes/form-css.php");
include("includes/sidebar.php");
    
    $page_title = "Instruction";
    $page_title_details = "Please controller with unique email and password with all details for keeping controller dashboard safe. if any types of problem please contact with me $xenerit_email";
?>

<!-- Form row -->
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><?php echo $page_title;?></h4>
            <p class="text-muted m-b-30 font-13">
                <?php echo $page_title_details;?>

            </p>

            <form method="post" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4" class="col-form-label">Project Title</label>
                        <input type="text" name="project_name" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4" class="col-form-label">Project By</label>
                        <input type="text" name="project_by" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress" class="col-form-label">Client Name</label>
                        <input type="text" name="client_name" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputAddress2" class="col-form-label">Date</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="dates" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="ti-calendar"></i></span>
                            </div>
                        </div><!-- input-group -->
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputAddress2" class="col-form-label">Project Link</label>
                        <input type="text" name="link" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputZip" class="col-form-label">logo</label>
                        <input type="file" name="logo" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip" class="col-form-label">Small Image</label>
                        <input type="file" name="small_image" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip" class="col-form-label">large Image</label>
                        <input type="file" name="large_image" class="form-control" required>
                    </div>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputCity" class="col-form-label">Skills</label>
                        <input type="text" name="skill" class="form-control" required>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputZip" class="col-form-label">Details</label>
                        <input type="text"  name="details" class="form-control" required>
                    </div>

                </div>
                <input type="submit" name="submit" value="Create User" class="btn btn-primary" class="btn btn-primary form-control">
            </form>
        </div>
    </div>
</div>

<?php

if(isset($_POST['submit'])){
$project_name = $_POST['project_name'];
$project_by = $_POST['project_by'];
$client_name = $_POST['client_name'];
$dates = $_POST['dates'];
$link = $_POST['link'];
$skill = $_POST['skill'];
$details = $_POST['details'];


$target_dir = "assets/pic/project_image/";
$logo = $target_dir .  date('d_m_Y_H_i_s') . '_'. $_FILES["logo"]["name"];
$logo_show = date('d_m_Y_H_i_s') . '_'. $_FILES["logo"]["name"];
$uploadOk = 1;
$imageFileType = pathinfo($logo, PATHINFO_EXTENSION);
move_uploaded_file($_FILES["logo"]["tmp_name"], $logo);
    
    
$target_dir = "assets/pic/project_image/";
$small_image = $target_dir .  date('d_m_Y_H_i_s') . '_'. $_FILES["small_image"]["name"];
$small_image_show = date('d_m_Y_H_i_s') . '_'. $_FILES["small_image"]["name"];
$uploadOk = 1;
$imageFileType = pathinfo($small_image, PATHINFO_EXTENSION);
move_uploaded_file($_FILES["small_image"]["tmp_name"], $small_image);
    
    
$target_dir = "assets/pic/project_image/";
$large_image = $target_dir .  date('d_m_Y_H_i_s') . '_'. $_FILES["large_image"]["name"];
$large_image_show = date('d_m_Y_H_i_s') . '_'. $_FILES["large_image"]["name"];
$uploadOk = 1;
$imageFileType = pathinfo($large_image, PATHINFO_EXTENSION);
move_uploaded_file($_FILES["large_image"]["tmp_name"], $large_image);
    
  

$insert = "INSERT INTO projects(project_name, project_by, client_name, dates, link, logo, small_image, large_image, skill, details) VALUES ('$project_name','$project_by','$client_name','$dates','$link','$logo_show','$small_image_show','$large_image_show','$skill','$details')";
$run = mysqli_query($con,$insert);

if($run){
echo "<script>window.open('view_project.php','_self')</script>";
}else
{
   echo"<script>alert('Error')</script>" ;
}}?>
<!-- end row -->

<?php 
include("includes/footer.php"); 
include("includes/form_js.php"); 
} ?>
