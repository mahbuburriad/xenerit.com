<?php
session_start();
include("includes/connection.php");
include("includes/settings.php"); 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $meta_description?>">
    <meta name="author" content="<?php echo $meta_author?>">
    <link rel="shortcut icon" href="<?php echo $favicon?>">
    <title><?php echo $company_full_name?> Management System | <?php echo $company_full_name?> | <?php echo $xenerit?></title>
        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
        <script src="assets/js/modernizr.min.js"></script>
    </head>

    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="ex-page-content text-center">
                <div class="text-error">500</div>
                <h3 class="text-uppercase font-600">Internal Server Error</h3>
                <p class="text-muted">
                    Why not try refreshing your page? or you can contact <a href="" class="text-primary"><?php echo $xenerit_email?></a>
                </p>
                <br>
                <a class="btn btn-success waves-effect waves-light" href="index.php"> Return Home</a>

            </div>
        </div>
        <!-- end wrapper page -->


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
	
	</body>
</html>