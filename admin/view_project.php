<?php
session_start();
$title ="Admins List";
include("includes/connection.php");
if(!isset($_SESSION['admin_email'])){
echo "<script>window.open('login.php', '_self')</script>";
}
else{
include("includes/index-header.php");
include("includes/table-css.php");
include("includes/sidebar.php");
    
    $page_title = "Instruction";
    $page_title_details = "Please controller with unique email and password with all details for keeping controller dashboard safe. if any types of problem please contact with me $xenerit_email";
?>


<div class="row">
    <div class="col-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><?php echo $page_title; ?></h4>
            <p class="text-muted font-14 m-b-30">
                <?php echo $page_title_details; ?>
            </p>

            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Project Name</th>
                        <th>Project By</th>
                        <th>Client Name</th>
                        <th>Date</th>
                        <th>Link</th>
                        <th>Skill</th>
                        <th>Details</th>
                        <th>Logo</th>
                        <th>Small Image</th>
                        <th>Large Image</th>
                        <th>Modify</th>
                    </tr>
                </thead>


                <tbody>
                    <?php
    $i=0;
$get_project = "SELECT project_id, project_name, project_by, client_name, dates,link, logo, small_image, large_image, skill, details FROM projects order by project_id desc";
$run_project = mysqli_query($con,$get_project);
while($row_project = mysqli_fetch_array($run_project)){
$project_id = $row_project['project_id'];
$project_name = $row_project['project_name'];
$project_by = $row_project['project_by'];
$client_name = $row_project['client_name'];
$dates = $row_project['dates'];
$link = $row_project['link'];
$logo = $row_project['logo'];
$small_image = $row_project['small_image'];
$large_image = $row_project['large_image'];
$skill = $row_project['skill'];
$details = $row_project['details'];
    $i++;
?>

                    <tr>
                        <td><?php echo $i++; ?></td>

                        <td>
                            <?php echo $project_name; ?>
                        </td>

                        <td>
                            <?php echo $project_by; ?>
                        </td>
                        <td>
                            <?php echo $client_name; ?>
                        </td>
                        <td>
                            <?php echo $dates; ?>
                        </td>

                        <td>
                            <?php echo $link; ?>
                        </td>
                        <td>
                            <?php echo $skill; ?>
                        </td>
                        <td>
                            <?php echo $details; ?>
                        </td>
                        <td>
                            <img style="height: 60px; width:60px" src="assets/pic/project_image/<?php echo $logo; ?>" alt="<?php echo $project_name;?>">
                        </td>
                        <td>
                            <img style="height: 60px; width:60px" src="assets/pic/project_image/<?php echo $small_image; ?>" alt="<?php echo $project_name;?>">
                        </td>
                        <td>
                            <img style="height: 60px; width:60px" src="assets/pic/project_image/<?php echo $large_image; ?>" alt="<?php echo $project_name;?>">
                        </td>

                        <td>

                            <a href="update_user.php?update=<?php echo $project_id; ?>"><i class="fa fa-pencil-square-o"></i></a> / <a href="delete_user.php?delete=<?php echo $project_id; ?>"><i class="fa fa-trash-o"></i></a>

                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- end row -->

<?php 
include("includes/footer.php"); 
include("includes/table_js.php"); 
    
    
} ?>
