<?php
session_start();
$title = "Update Profile";
include("includes/connection.php");
if(!isset($_SESSION['admin_email'])){
echo "<script>window.open('login.php', '_self')</script>";
}
else{
include("includes/index-header.php");
include("includes/form-css.php");
include("includes/sidebar.php");
    
$page_title = "Instruction";
$page_title_details = "Please controller with unique email and password with all details for keeping controller dashboard safe. if any types of problem please contact with me $xenerit_email";

if(isset($_GET['update'])){
$edit_id = $_GET['update'];
$get_admins = "select * from admins where admin_id='$edit_id'";
$run_admins = mysqli_query($con,$get_admins);
$row_admins = mysqli_fetch_array($run_admins);
$admin_ids = $row_admins['admin_id'];
$admin_names = $row_admins['admin_name'];
$admin_emails = $row_admins['admin_email'];
$admin_passs = $row_admins['admin_pass'];
$admin_images = $row_admins['admin_image'];
$admin_countrys = $row_admins['admin_country'];
$admin_jobs = $row_admins['admin_job'];
$admin_contacts = $row_admins['admin_contact'];
$admin_abouts = $row_admins['admin_about'];
}
?>
<!-- Form row -->
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><?php echo $page_title;?></h4>
            <p class="text-muted m-b-30 font-13">
                <?php echo $page_title_details;?>
            </p>
            <form method="post" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4" class="col-form-label"><?php echo $admin_area_name?> Name</label>
                        <input type="text" value="<?php echo $admin_names?>" name="admin_name" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label"><?php echo $admin_area_name?> Email</label>
                        <input type="email" name="admin_email" value="<?php echo $admin_emails?>" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label"><?php echo $admin_area_name?> Password</label>
                        <input type="password" name="admin_pass" value="<?php echo $admin_passs?>" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress2" class="col-form-label"><?php echo $admin_area_name?> Address</label>
                        <input type="text" name="admin_country" value="<?php echo $admin_countrys?>" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCity" class="col-form-label"><?php echo $admin_area_name?> Job/Position</label>
                        <input type="text" name="admin_job" value="<?php echo $admin_jobs?>" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip" class="col-form-label"><?php echo $admin_area_name?> Contact</label>
                        <input type="tel" name="admin_contact" value="<?php echo $admin_contacts?>" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputZip" class="col-form-label"><?php echo $admin_area_name?> About</label>
                    <textarea name="admin_about" class="form-control" rows="3"><?php echo $admin_abouts ?> </textarea>
                </div>
                <input type="submit" name="update" value="Update Controller" class="btn btn-primary" class="btn btn-primary form-control">
            </form>
        </div>
    </div>
</div>

<?php
if(isset($_POST['update'])){
$admin_name = $_POST['admin_name'];
$admin_email = $_POST['admin_email'];
$admin_pass = $_POST['admin_pass'];
$admin_country = $_POST['admin_country'];
$admin_job = $_POST['admin_job'];
$admin_contact = $_POST['admin_contact'];
$admin_about = $_POST['admin_about'];
    
$update_admin = "update admins set admin_name='$admin_name',admin_email='$admin_email',admin_pass='$admin_pass',admin_contact='$admin_contact',admin_job='$admin_job',admin_about='$admin_about' where admin_id='$admin_ids'";

$run_admin = mysqli_query($con,$update_admin);
if($run_admin){
echo "<script>window.open('logout.php','_self')</script>";

}
{
   echo"<script>alert('Error')</script>" ;
}
}

include("includes/footer.php"); 
include("includes/form_js.php"); 
} ?>
