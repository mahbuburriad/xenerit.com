<?php
include("includes/connection.php");
include("includes/settings.php");

if(isset($_GET['reset_password'])){
$edit_id = $_GET['reset_password'];
$get_admins = "select * from admins where reset_code='$edit_id'";
$run_admins = mysqli_query($con,$get_admins);
$count = mysqli_num_rows($run_admins); 
    
    if($count==1){

$row_admins = mysqli_fetch_array($run_admins);
$reset_code = $row_admins['reset_code'];


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title><?php echo $company_full_name ?> - <?php echo $company_tag_line?></title>

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    </head>

    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="index.php" class="logo"><span><?php echo $company_name_1st_part ?><span><?php echo $company_name_2nd_part ?></span></span></a>
                <h5 class="text-muted m-t-0 font-600"><?php echo $company_tag_line?></h5>
            </div>
        	<div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Update Password</h4>

					<p class="text-muted m-b-0 font-13 m-t-20">Enter unique and very strong password with lowercase, uppercase and character mix.  </p>
                </div>
                <div class="p-20">
                    <form class="form-horizontal m-t-20" method="post" enctype="multipart/form-data">

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" name="admin_pass" placeholder="Enter password" required>
                            </div>
                        </div>

                        <div class="form-group text-center m-t-20 m-b-0">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" name="submit" type="submit">Update Password</button>
                            </div>
                        </div>
                        
                        <p style="color: green" id="line">Your Password has been reset successfully</p>

                    </form>

                </div>
            </div>
            <!-- end card-box -->

			<div class="row">
				<div class="col-sm-12 text-center">
					<p class="text-muted">Already have account?<a href="login.php" class="text-primary m-l-5"><b>Sign In</b></a></p>
				</div>
			</div>

        </div>
        <!-- end wrapper page -->


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
            <script>
        $("#line").hide();


    </script>
        <?php
if(isset($_POST['submit'])){
$admin_pass = $_POST['admin_pass'];
 
$update_admin = "update admins set admin_pass='$admin_pass', reset_code='' where reset_code = '$edit_id'";

$run_admin = mysqli_query($con,$update_admin);
if($run_admin){
        echo "
    
    <script>
$('#line').show();
</script>
    
    ";
    echo "<script>window.open('login.php','_self')</script>";

}}}
    
 else{
    echo "<script>window.open('login.php','_self')</script>";
}   

}

?>
	</body>
</html>